#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 20 15:42:15 2018

@author: shabani
"""
import datetime
import pymongo
from pymongo import MongoClient
from bson.code import Code
import dateutil.parser
import json
import os
from datetime import date

def rawDataNewCollection(new_database,old_database,collection,new_collection):
    fileName= "/home/shabani/Data_Processing/data/sensors_cache/"+new_collection
    try:
        os.remove(fileName)
    except FileNotFoundError:
        pass
    
    
    client = MongoClient()
    client[new_database].drop_collection(new_collection)
    client[new_database][new_collection].create_index("timestamp")
    
    data = client[old_database][collection].find()
    countAll =client[old_database][collection].count()
    print(str(countAll)+" Documents to format")
    count=0
    memoryTimestamp = ''

    for doc in data:
        isNewMinute = True
        timestamp = doc["date_and_time"]
        timestamp = timestamp[:-7]+':'+'00'
        if (timestamp==memoryTimestamp):
            isNewMinute = False
        memoryTimestamp=timestamp
        formated_doc = {"timestamp":dateutil.parser.parse(timestamp)}
        for key in doc.keys():
            if((key != 'date_and_time') & (key!='_id')):
                keyWithoutDot = key.replace(".", "")
                formated_doc[keyWithoutDot]=doc[key]
                
        if(isNewMinute):
            client[new_database][new_collection].update({'timestamp':formated_doc['timestamp']}, {"$set": formated_doc}, upsert=True)
            count=count+1
        if(count%100000 == 0):
            print(str(count)+" formated...")
            
    countAllNew=client[new_database][new_collection].count()        
    print("Finished, "+ str(count) +" documents formated")
    print("Stats : \nFrom : "+str(countAll)+ " to " + str(countAllNew) +" documents")
    
def rawDataUpdate(new_database,old_database,collection,newCollection):
    fileName= "/home/shabani/Data_Processing/data/collections_cache/"+newCollection
        
    client = MongoClient()
    #client[new_database].drop_collection(new_collection)
    #client[new_database][new_collection].create_index("timestamp")
    
    data = client[old_database][collection].find()#{},{"_id":False,"date_and_time":False, "record":False})
    #old_data=client[new_database][newCollection].find({},{"timestamp":True,"_id":False})
    old_keys=client[new_database][newCollection].find_one()
    new_keys=client[old_database][collection].find_one()
    
    oldestDataOldCollection =client[new_database][newCollection].find().sort('timestamp', pymongo.ASCENDING).limit(1)
    for d in oldestDataOldCollection:
        oldestDataOldCollection= d["timestamp"]
    newestDataOldCollection =client[new_database][newCollection].find().sort('timestamp', pymongo.DESCENDING).limit(1)
    for d in newestDataOldCollection:
        newestDataOldCollection= d["timestamp"]
        
    print("The oldest date :"+str(oldestDataOldCollection))
    print("The newest date :"+str(newestDataOldCollection)) 
    count_changes=0
    memoryTimestamp = ''
    for old_key in old_keys:
        if(old_key!="timestamp" and old_key!= "id"):
            for doc in data:
                isNewMinute = True
                timestamp = doc["date_and_time"]
        
                timestamp = timestamp[:-7]+':'+'00'
                if (timestamp==memoryTimestamp):
                    isNewMinute = False
                memoryTimestamp=timestamp
                formated_doc = {"timestamp":dateutil.parser.parse(timestamp)}
                for new_key in new_keys:
                    if((oldestDataOldCollection > dateutil.parser.parse(timestamp)) or (newestDataOldCollection < dateutil.parser.parse(timestamp)) or( old_key != new_key)):
                        
                        for key in doc.keys():
                            if((key != 'date_and_time') & (key!='_id')):
                                keyWithoutDot = key.replace(".", "")
                                formated_doc[keyWithoutDot]=doc[key]          
                        if(isNewMinute):
                            count_changes=count_changes +1
                            client[new_database][newCollection].update({'timestamp':formated_doc['timestamp']}, {"$set": formated_doc}, upsert=True)
                        
                        if(count_changes%100000 == 0):
                            print(str(count_changes)+" updating...")               
              
    print(str(count_changes)+ " times updated the database")
    
def collectionsCache(new_database,collection): 
    client = MongoClient()
    fileName= "/home/shabani/Data_Processing/data/sensors_cache/"+collection
    mapper = Code("function () {"
                  "for (var key in this) {"
                  "emit(key, null);}"
                  "}")
    reducer = Code("function(key, stuff) { return null; }")
    distinctThingFields = client[new_database][collection].map_reduce(mapper, reducer, out = {'inline' : 1}, full_response = True)
    key_list = []
    for key_object in distinctThingFields[u'results']:
        key_list.append(key_object[u'_id'])
    
    data2write = {"keys":[]}
    for key in key_list:
        if((key!="_id") & (key!="timestamp")):
            data2write["keys"].append(key)
    
    with open(fileName, 'w') as outfile:
        json.dump(data2write, outfile)
   

def sensorDefaultConfig(collection):
    client = MongoClient()
    fileName= "/home/shabani/Data_Processing/data/sensors_cache/"+collection
    with open(fileName, 'r') as infile:
        data = json.load(infile)
        for key in data["keys"]:
            client["configuration"]["sensors"].update({"name":key}, {"name":key, "method":"Average", "clustering":{}}, upsert=True)

def calendarFormat(database, collection):
    client = MongoClient()
    data = client[database][collection].find()
    for doc in data:
        if doc["timestamp"]:
            timestamp= doc["timestamp"]
            year=str(timestamp).split("-")[0]
            month=str(timestamp).split("-")[1]
            d=str(timestamp).split("-")[2]
            dayMonth=d.split(" ")[0]
            weekYear= date.isocalendar(timestamp)[1]
            weekday= date.isocalendar(timestamp)[2]
            la_date= timestamp.date()
            time=timestamp.ctime()
            timea=time.split(" ")[3]
            if(timea== "1" or timea=="2" or timea== "3" or timea=="4" or timea== "5" or timea=="6" or timea== "7" or timea=="8" or timea =="9"):
                timed=time.split(" ")[4]
                hour=timed.split(":")[0]
                minute=timed.split(":")[1]
                minute=minute.split(":")[0]
            else:
                hour=timea.split(":")[0]
                minute=timea.split(":")[1]
                minute=minute.split(":")[0]
            seconds="00"
            fusion_hour_minute=hour+minute
            client[database][collection].update({'timestamp':timestamp},{"$set":{"Year":year,"Quarter":calculeTrimestre(month),"Week_of_year":weekYear,"Month":month,"Month_name":calculeNomMois(month),"Day_of_month":dayMonth,"Week_of_Month":calculWeekofMonth(dayMonth),"Day_of_week":weekday,"Day_name":calculeNomJour(weekday),"Date":str(la_date), "Hour": int(fusion_hour_minute),"Time":hour+":"+minute+":"+seconds}})
    
    #print("Year: " +str(year) +" month: "+str(month)+" day: "+str(dayMonth)+" weeknumber: "+str(weekYear)+" weekday: " +str(weekday) +" trimestre: " +str(calculeTrimestre(month)))

def calculWeekofMonth(d):
    week=0
    day= float(d)/31
    if day >= 0.03 and day <=0.227:
        week=1
    elif day >= 0.258 and day <= 0.452:
        week=2
    elif day >= 0.482 and day <= 0.679:
        week=3
    elif day >= 0.707 and day<= 0.904:
        week=4
    elif day >= 0.933 and day<= 1.13:
        week=5
    return week
     
def calculeNomJour(jour):
    if(jour == 1):
        day="Monday"
    elif(jour==2):
        day="Tuesday"
    elif(jour==3):
        day="Wednesday"
    elif(jour==4):
        day="Thursday"
    elif(jour==5):
        day="Friday"
    elif(jour==6):
        day="Saturday"
    elif(jour==7):
        day="Sunday"
    return day
        
def calculeNomMois(mois):
    month=""
    if(mois == "01"):
        month="January"
    elif(mois == "02"):
        month="Febrary"
    elif(mois == "03"):
        month="March"
    elif(mois == "04"):
        month="April"
    elif(mois == "05"):
        month="May"
    elif(mois == "06"):
        month="June"
    elif(mois == "07"):
        month="July"
    elif(mois == "08"):
        month="August"
    elif(mois == "09"):
        month="September"
    elif(mois == "10"):
        month="October"
    elif(mois == "11"):
        month="November"
    elif(mois == "12"):
        month="December"
    return month

        
def calculeTrimestre(mois):
    trimestre= float(mois)/4
    tri=0
    if trimestre >=0.25 and trimestre <= 0.75:
        tri=1
    elif trimestre >= 1 and trimestre <= 1.5:
        tri=2
    elif trimestre >= 1.75 and trimestre <=2.25:
        tri=3
    elif trimestre >=2.25 and trimestre <= 3:
        tri=4
    
    return tri
"""            
import Importer_les_data
Importer_les_data.vp_start_gui()
"""