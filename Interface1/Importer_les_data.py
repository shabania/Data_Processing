#! /usr/bin/env python
#
# GUI module generated by PAGE version 4.12
# In conjunction with Tcl version 8.6
#    Apr 19, 2018 04:07:16 PM

import sys

try:
    from Tkinter import *
except ImportError:
    from tkinter import *

try:
    import ttk
    py3 = False
except ImportError:
    import tkinter.ttk as ttk
    py3 = True
    
import tkinter.tix
#import scrolledlistbox_support
import Importer_les_data_support as support

def vp_start_gui():
    #Starting point when module is the main routine.
    global val, w, root
    root = Tk()
    support.set_Tk_var()
    top = New_Toplevel (root)
    support.init(root, top)
    root.mainloop()

w = None
def create_New_Toplevel(root, *args, **kwargs):
    #Starting point when module is imported by another program.
    global w, w_win, rt
    rt = root
    w = Toplevel (root)
    support.set_Tk_var()
    top = New_Toplevel (w)
    support.init(w, top, *args, **kwargs)
    return (w, top)

def destroy_New_Toplevel():
    #Destroy window
    global w
    w.destroy()
    w = None

class New_Toplevel:
    def __init__(self, top=None):
        #This class configures and populates the toplevel window.
        #top is the toplevel containing window
        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana1color = '#d9d9d9' # X11 color: 'gray85' 
        _ana2color = '#d9d9d9' # X11 color: 'gray85' 
        font9 = "-family {DejaVu Sans} -size 13 -weight bold -slant "  \
            "roman -underline 0 -overstrike 0"
        font12= "-family {DejaVu Sans} -size 8 -weight normal -slant roman -underline 0 -overstrike 0"
        self.style = ttk.Style()
        if sys.platform == "win32":
            self.style.theme_use('winnative')
        self.style.configure('.',background=_bgcolor)
        self.style.configure('.',foreground=_fgcolor)
        self.style.configure('.',font="TkDefaultFont")
        self.style.map('.',background=
            [('selected', _compcolor), ('active',_ana2color)])

        top.geometry("986x754+491+82")
        top.title("Import/Format Data")
        top.configure(highlightcolor="black")
        #top.iconbitmap(r'icon.gif')

        """
        button1 = tkinter.tix.Button(top, text='Something Unexpected')
        button1.place(relx=0.03, rely=0.15, height=18, width=90)
        status = Label(root, height = 3, width=30, bd=1,bg='yellow',wraplength = 210, text = "All angles are in degrees")
        
        a=tix.Balloon(top)
        """
        
        self.Canvas_Import = Canvas(top)
        self.Canvas_Import.place(relx=0.19, rely=0.05, relheight=0.17
                , relwidth=0.61)
        self.Canvas_Import.configure(borderwidth="2")
        self.Canvas_Import.configure(relief=RIDGE)
        self.Canvas_Import.configure(selectbackground="#c4c4c4")
        self.Canvas_Import.configure(width=601)
        
       
        
        #Label for CSV
        self.Label_BrowseCsv = Label(self.Canvas_Import)
        self.Label_BrowseCsv.place(relx=0.03, rely=0.15, height=18, width=90)
        self.Label_BrowseCsv.configure(activebackground="#f9f9f9")
        self.Label_BrowseCsv.configure(text='''Browse Csv :''')
        
        #Label for Folder
        self.Label_BrowseFolder = Label(self.Canvas_Import)
        self.Label_BrowseFolder.place(relx=0.03, rely=0.47, height=18, width=106)
        self.Label_BrowseFolder.configure(activebackground="#f9f9f9")
        self.Label_BrowseFolder.configure(text='''Browse Folder :''')
                      
        
        #Label for Folder
        self.Label_Separator = Label(self.Canvas_Import)
        self.Label_Separator.place(relx=0.03, rely=0.71, height=18, width=80)
        self.Label_Separator.configure(activebackground="#f9f9f9")
        self.Label_Separator.configure(text="Separator :")
        
        def separatorComboBoxCheck(event):
            support.separatorCheck()
        #Combobox Houses 
        support.TCombobox_Separator = ttk.Combobox(self.Canvas_Import)
        support.TCombobox_Separator.place(relx=0.22, rely=0.71, relheight=0.14, relwidth=0.08)
        support.TCombobox_Separator.configure(textvariable=support.separatorcombobox, value=support.separatorComboValue)
        support.TCombobox_Separator.configure(takefocus="")
        
        def on_enter2(event):
            self.Message3.place(relx=0.20, rely=0.20, relheight=0.04, relwidth=0.35)
            self.Message3.configure(text='''To know the seperator, you should check the Csv file.
Default separator is "," ''')
            support.TCombobox_Separator.configure(background="#ff0000")

        def on_leave2(enter):
            self.Message3.place_forget()

        self.Message3 = Message(top)
        self.Message3.configure(background="#f3f691")
        self.Message3.configure(font=font12)
        self.Message3.configure(width=400)                   
        self.Message3.configure(borderwidth=0.8, relief=SOLID)
        
        support.TCombobox_Separator.bind("<<ComboboxSelected>>",separatorComboBoxCheck)
        support.TCombobox_Separator.bind("<Enter>", on_enter2)
        support.TCombobox_Separator.bind("<Leave>", on_leave2)
        
        #Entry for Folder path
        support.Entry_Separator = Entry(self.Canvas_Import)
        #support.Entry_Separator.place(relx=0.33, rely=0.707,height=19.5, relwidth=0.05)
        support.Entry_Separator.configure(background="white")
        support.Entry_Separator.configure(font="TkFixedFont")
        support.Entry_Separator.configure(selectbackground="#c4c4c4")
        support.Entry_Separator.configure(textvariable=support.entrySeparator)
        
        #Entry for CSV path
        self.Entry_Csv = Entry(self.Canvas_Import)
        self.Entry_Csv.place(relx=0.22, rely=0.15,height=20, relwidth=0.49)
        self.Entry_Csv.configure(background="white")
        self.Entry_Csv.configure(font="TkFixedFont")
        self.Entry_Csv.configure(selectbackground="#c4c4c4")
        self.Entry_Csv.configure(textvariable=support.entryCsv)

        #Entry for Folder path
        self.Entry_Folder = Entry(self.Canvas_Import)
        self.Entry_Folder.place(relx=0.22, rely=0.46,height=20, relwidth=0.49)
        self.Entry_Folder.configure(background="white")
        self.Entry_Folder.configure(font="TkFixedFont")
        self.Entry_Folder.configure(selectbackground="#c4c4c4")
        self.Entry_Folder.configure(textvariable=support.entryFolder)
    

        #Button Browse for CSV
        self.Button_BrowseCsv = Button(self.Canvas_Import)
        self.Button_BrowseCsv.place(relx=0.77, rely=0.15, height=26, width=107)
        #support.img1 = PhotoImage(file="icon.png")
        #self.Button_BrowseCsv.configure(image=support.img1)
        self.Button_BrowseCsv.configure(activebackground="#d9d9d9")
        self.Button_BrowseCsv.configure(command=support.fill_entry_csvpath)
        self.Button_BrowseCsv.configure(text='''Browse Csv''')
        
        
        #Button Browse for Folder
        self.Button_BrowseFolder = Button(self.Canvas_Import)
        self.Button_BrowseFolder.place(relx=0.77, rely=0.46, height=26, width=107)
        self.Button_BrowseFolder.configure(activebackground="#d9d9d9")
        self.Button_BrowseFolder.configure(command=support.fill_entry_folderpath)
        self.Button_BrowseFolder.configure(text='''Browse Folder''')

        #Button to refrech entry fields
        self.Button_RefreshEntry = Button(self.Canvas_Import)
        self.Button_RefreshEntry.place(relx=0.40, rely=0.69, height=26, width=45)
        self.Button_RefreshEntry.configure(activebackground="#d9d9d9")
        self.Button_RefreshEntry.configure(command=support.refreshBrowseEntry)
        self.Button_RefreshEntry.configure(text='''Clear''')
        
        #Button to upload Csv files
        self.Button_Upload = Button(self.Canvas_Import)
        self.Button_Upload.place(relx=0.52, rely=0.69, height=26, width=67)
        self.Button_Upload.configure(activebackground="#d9d9d9")
        self.Button_Upload.configure(command=support.uploadCsv)
        self.Button_Upload.configure(text='''Upload''')

        #II.Canvas to FormatData
        self.Canvas_Formate = Canvas(top)
        self.Canvas_Formate.place(relx=0.03, rely=0.27, relheight=0.66, relwidth=0.93)
        self.Canvas_Formate.configure(borderwidth="2")
        self.Canvas_Formate.configure(relief=RIDGE)
        self.Canvas_Formate.configure(selectbackground="#c4c4c4")
        self.Canvas_Formate.configure(width=921)
        
        #II.I Canvas, to select a temporary collection and formatting type
        #This Canvas is inside the II.Formatdata Canvas
        self.Canvas_Select_tmp_coll = Canvas(self.Canvas_Formate)
        self.Canvas_Select_tmp_coll.place(relx=0.03, rely=0.06, relheight=0.28, relwidth=0.9)
        self.Canvas_Select_tmp_coll.configure(borderwidth="2")
        self.Canvas_Select_tmp_coll.configure(relief=RIDGE)
        self.Canvas_Select_tmp_coll.configure(selectbackground="#c4c4c4")
        self.Canvas_Select_tmp_coll.configure(width=601)
        
        #Text to select a temporary collection
        self.Message1 = Message(self.Canvas_Select_tmp_coll)
        self.Message1.place(relx=0.0035, rely=0.11, relheight=0.15, relwidth=0.20)
        self.Message1.configure(text="Select a collection to format")
        self.Message1.configure(width=145)
        
        self.Label1 = Label(self.Canvas_Select_tmp_coll)
        self.Label1.place(relx=0.14, rely=0.22, relheight=0.1, relwidth=0.025)
        #self._img1 = PhotoImage(file="Info_icon_002.svg.png")
        #self.Label1.configure(image=self._img1)
        self.Label1.configure(borderwidth=0.8, relief=SOLID)
        self.Label1.configure(background="#f3f691")
        self.Label1.configure(text="i")
        self.Label1.configure(width=10)
        
        def on_enter(event):
            self.Message1.place(relx=0.006, rely=0.345, relheight=0.62, relwidth=0.20)
            self.Message1.configure(text="These are temporary collections:\n(f): means that collection has been formated\n* : number of times formatted",  justify='left')
            self.Label1.configure(background="#ff0000")
        def on_leave(enter):
            self.Message1.place_forget()
            self.Label1.configure(background="#f3f691")

        self.Message1 = Message(self.Canvas_Select_tmp_coll)
        self.Message1.configure(background="#f3f691")
        self.Message1.configure(width=153)
        
        self.Label1.bind("<Enter>", on_enter)
        self.Label1.bind("<Leave>", on_leave)

        
        #function to get the value of the selected collection when it's double-clicked
        def doubleselect(event):
            support.tmpCollectionSelected() #Function defined in "Importer_les_data_support.py"
            
        #ListBox to display collections from the temporary database 
        support.Listbox_temporaryCollection = Listbox(self.Canvas_Select_tmp_coll)
        support.Listbox_temporaryCollection.place(relx=0.22, rely=0.09, relheight=0.6, relwidth=0.22)
        support.Listbox_temporaryCollection.configure(background="white")
        support.Listbox_temporaryCollection.configure(font="TkFixedFont")
        support.Listbox_temporaryCollection.configure(selectbackground="#c4c4c4")
        support.Listbox_temporaryCollection.configure(width=134)
        support.Listbox_temporaryCollection.bind("<Double-Button-1>",doubleselect) 
        support.Listbox_temporaryCollection.configure(listvariable=support.temp_collections)

        #Button to delete a collection in the temporary database
        self.Button_DelleteCollection = Button(self.Canvas_Select_tmp_coll)
        self.Button_DelleteCollection.place(relx=0.22, rely=0.71, height=26, width=125)
        self.Button_DelleteCollection.configure(activebackground="#d9d9d9")
        self.Button_DelleteCollection.configure(command=support.deleteCollection)
        self.Button_DelleteCollection.configure(text='''Delete''')
        
        #Button to confirm a selected collection in the temporary database
        self.Button_OkCollection = Button(self.Canvas_Select_tmp_coll)
        self.Button_OkCollection.place(relx=0.375, rely=0.71, height=26, width=55)
        self.Button_OkCollection.configure(activebackground="#d9d9d9")
        self.Button_OkCollection.configure(command=support.tmpCollectionSelected)
        self.Button_OkCollection.configure(text="Select")
        
        #Label select collection
        support.Labelinfo = Label(self.Canvas_Select_tmp_coll)
        support.Labelinfo.configure(activebackground="#f9f9f9")
        support.Labelinfo.configure(text="Selected Collection:")
        
        #Label to get the name of the selected collection when doubble click on collection or button "done" click
        support.Label3 = Label(self.Canvas_Select_tmp_coll)
        support.Label3.configure(activebackground="#f9f9f9")
        support.Label3.configure(textvariable=support.temporaryCollection)
        
        #Label to choose a formatting type
        support.Label33 = Label(self.Canvas_Select_tmp_coll)
        support.Label33.configure(activebackground="#f9f9f9")
        support.Label33.configure(text='''Formatting Type :''')
        
        def formattingBoxUpdate(event):
            support.loadNewPython()
            
        
        #Combobox Houses 
        support.TCombobox_Houses = ttk.Combobox(self.Canvas_Select_tmp_coll)
        support.TCombobox_Houses.bind("<<ComboboxSelected>>",formattingBoxUpdate)
        support.TCombobox_Houses.configure(textvariable=support.pythoncombobox, value=support.formattingComboValue)
        support.TCombobox_Houses.configure(takefocus="")
        
        #Entry for new formatting python script
        support.EntryNewPython = Entry(self.Canvas_Select_tmp_coll)
        support.EntryNewPython.configure(background="white")
        support.EntryNewPython.configure(font="TkFixedFont")
        support.EntryNewPython.configure(selectbackground="#c4c4c4")
        support.EntryNewPython.configure(textvariable=support.newPythonScript)
        
        support.ButtonScript = Button(self.Canvas_Select_tmp_coll)
        support.ButtonScript.configure(activebackground="#f9f9f9")
        support.ButtonScript.configure(command=support.fill_EntryScript)                            
        support.ButtonScript.configure(text="Browse")
        
        support.Label2 = Label(self.Canvas_Select_tmp_coll)
        #support._img1 = PhotoImage(file="Info_icon_002.svg.png")
        #support.Label1.configure(image=self._img1)
        support.Label2.configure(borderwidth=0.8, relief=SOLID)
        support.Label2.configure(background="#f3f691")
        support.Label2.configure(text="*")
        
        
        def on_enter1(event):
            self.Message2.place(relx=0.40, rely=0.40, relheight=0.13, relwidth=0.51)
            self.Message2.configure(text='''* a python script name is required without extention '.py'.
                                    
Make sure that your new script contains 4 functions called : 
-rawDataUpdate(new_database,tmp_database,tmp_collection,new_Collection)
-rawDataNewCollection(new_database,tmp_database,tmp_collection,new_Collection)
-collectionCache(new_database,tmp_collection)
-sensorDefaultConfig(tmp_collection)''')
            support.Label2.configure(background="#ff0000")

        def on_leave1(enter):
            self.Message2.place_forget()
            support.Label2.configure(background="#f3f691")

        self.Message2 = Message(top)
        self.Message2.configure(background="#f3f691")
        self.Message2.configure(font=font12)
        self.Message2.configure(width=600)                   
        self.Message2.configure(borderwidth=0.8, relief=SOLID)
        
        support.Label2.bind("<Enter>", on_enter1)
        support.Label2.bind("<Leave>", on_leave1)

        #II.II.Canvas for saving the collection
        #This canvas is inside the II.FormatData Canvas
        #In this Canvas we choose where to save collection and how to name it
        support.Canvas_Savein = Canvas(self.Canvas_Formate)
        support.Canvas_Savein.configure(borderwidth="2")
        support.Canvas_Savein.configure(relief=RIDGE)
        support.Canvas_Savein.configure(selectbackground="#c4c4c4")
        support.Canvas_Savein.configure(width=831)

        

        #Label for selection a Database
        support.Label_selectDb = Label(support.Canvas_Savein)
        support.Label_selectDb.configure(activebackground="#f9f9f9")
        support.Label_selectDb.configure(text='''Select DB :''')
        
        
        #self.panel.pack(side = "bottom", fill = "both", expand = "yes")
        #Label for selecting a collection 
        support.Label_SelectCollection = Label(support.Canvas_Savein)
        support.Label_SelectCollection.configure(activebackground="#f9f9f9")
        support.Label_SelectCollection.configure(text='''Select collection :''')
        
        #Function to display collection, depending on the database selected in the combobox
        def TextBoxUpdate(event):
            support.loadDbListeBox() #Fuction coded in "Importer_les_data_support.py"
        #def comboboxCollectionUpdate(event):
         #   support.loadCollListeBox()
            
        #Function to display the Entry if in listbox collections name "Create_New" is selected
        #def Entryshow(event):
            #support.placeEntry() #Fuction coded in "Importer_les_data_support.py"
       
        #Collections Listbox depenfing on the database selected in the combobox

        
        #Button to delete a database
        support.Button_DeleteColl = Button(support.Canvas_Savein)
        #support.Button_DeleteColl.place(relx=0.60, rely=0.08, height=26, width=130)
        support.Button_DeleteColl.configure(activebackground="#d9d9d9")
        support.Button_DeleteColl.configure(command=support.deleteColl)
        support.Button_DeleteColl.configure(text='''Delete Collection''')
        #.place(relx=0.35, rely=0.19, relheight=0.25, relwidth=0.21)
        
        #Button to delete a collection 
        support.Button_DeleteDB = Button(support.Canvas_Savein)
        #support.Button_DeleteDB.place(relx=0.60, rely=0.19, height=26, width=130)
        support.Button_DeleteDB.configure(activebackground="#d9d9d9")
        support.Button_DeleteDB.configure(command=support.deleteDb)
        support.Button_DeleteDB.configure(text='''Delete Database''')

        #Combobox for chosing a database
        support.TCombobox_SelectDB = ttk.Combobox(support.Canvas_Savein)
        support.TCombobox_SelectDB.bind("<<ComboboxSelected>>",TextBoxUpdate)
        support.TCombobox_SelectDB.configure(textvariable=support.comboboxDB, value=support.combovalueDB)
        support.TCombobox_SelectDB.configure(takefocus="")
        
        #Combobox for chosing collection
        support.TCombobox_SelectCollection = ttk.Combobox(support.Canvas_Savein)
        #support.TCombobox_SelectCollection.bind("<<ComboboxSelected>>",TextBoxUpdate)
        support.TCombobox_SelectCollection.configure(textvariable=support.comboboxCollection, value=support.combovalueCollection)
        support.TCombobox_SelectCollection.configure(takefocus="")
        
        #Label for the new collections name 
        support.Label_newName = Label(support.Canvas_Savein)
        support.Label_newName.configure(activebackground="#f9f9f9")
        support.Label_newName.configure(text=support.labeltext)

        #Entry for the new collections name
        support.Entry_NewCollName = Entry(support.Canvas_Savein)
        support.Entry_NewCollName.configure(background="white")
        support.Entry_NewCollName.configure(font="TkFixedFont")
        support.Entry_NewCollName.configure(selectbackground="#c4c4c4")
        support.Entry_NewCollName.configure(textvariable=support.entryNewCollName)

       
        #Checkbutton for saving in a New Database
        support.Checkbutton_NewDB = Checkbutton(support.Canvas_Savein)
        #support.Checkbutton_NewDB.place(relx=0.137, rely=0.19, relheight=0.08, relwidth=0.20)
        support.Checkbutton_NewDB.configure(activebackground="#d9d9d9")
        support.Checkbutton_NewDB.configure(justify=LEFT)
        support.Checkbutton_NewDB.configure(text='''Create new Database''')
        support.Checkbutton_NewDB.configure(variable=support.NewDB)
        support.Checkbutton_NewDB.configure(command=support.showNewDbEntry)
        #Checkbutton for saving in a New Database
        
        support.Checkbutton_NewColl = Checkbutton(support.Canvas_Savein)
        #support.Checkbutton_NewColl.place(relx=0.145, rely=0.50, relheight=0.08, relwidth=0.20)
        support.Checkbutton_NewColl.configure(activebackground="#d9d9d9")
        support.Checkbutton_NewColl.configure(justify=LEFT)
        support.Checkbutton_NewColl.configure(text='''Create new Collection''')
        support.Checkbutton_NewColl.configure(variable=support.NewColl)
        support.Checkbutton_NewColl.configure(command=support.showNewCollEntry)
        
        
        #Label for the new Database
        support.Label10 = Label(support.Canvas_Savein)
        support.Label10.configure(activebackground="#f9f9f9")
        support.Label10.configure(text='''New DB name :''')

        #Label for the Collections name
        support.Label11 = Label(support.Canvas_Savein)
        support.Label11.configure(activebackground="#f9f9f9")
        support.Label11.configure(text='''Collections name :''')

        #Entry field for the database name
        support.Entry_NewDbName = Entry(support.Canvas_Savein)
        support.Entry_NewDbName.configure(background="white")
        support.Entry_NewDbName.configure(font="TkFixedFont")
        support.Entry_NewDbName.configure(selectbackground="#c4c4c4")
        support.Entry_NewDbName.configure(textvariable=support.entryNewDbName)
                                                     
        #Label to Savin 
        support.Label_Savin = Label(self.Canvas_Formate)
        support.Label_Savin.place(relx=0.03, rely=0.34, height=24, width=99)
        support.Label_Savin.configure(activebackground="#f9f9f9")
        support.Label_Savin.configure(font=font9)
        support.Label_Savin.configure(text="Save in :")
        #Label to Savin 
        support.Label_Tmp = Label(self.Canvas_Formate)
        support.Label_Tmp.place(relx=0.03, rely=0.01, height=24, width=220)
        support.Label_Tmp.configure(activebackground="#f9f9f9")
        support.Label_Tmp.configure(font=font9)
        support.Label_Tmp.configure(text="Temporary Database :")
        #button1_ttp = CreateToolTip(support.Label_Tmp, "mouse is over button 1")
        
        
        #Button apply for launching the format
        support.Button_Apply = Button(self.Canvas_Formate)
        support.Button_Apply.place(relx=0.43, rely=0.92, height=26, width=100)
        support.Button_Apply.configure(activebackground="#d9d9d9")
        support.Button_Apply.configure(command=support.apply)
        support.Button_Apply.configure(state='disabled')
        support.Button_Apply.configure(text='''Apply''')
        
        """
        #Button Refrech for re-loading every fieled
        self.Boutton_Refresh = Button(self.Canvas_Formate)
        self.Boutton_Refresh.place(relx=0.37, rely=0.92, height=26, width=91)
        self.Boutton_Refresh.configure(activebackground="#d9d9d9")
        self.Boutton_Refresh.configure(command=support.refreshAllWindow)
        self.Boutton_Refresh.configure(text='''Refresh''')
"""
        #Label Format Data
        self.Label5 = Label(top)
        self.Label5.place(relx=0.02, rely=0.24, height=18, width=146)
        self.Label5.configure(activebackground="#f9f9f9")
        self.Label5.configure(font=font9)
        self.Label5.configure(text='''Format Data''')

        #Label import data
        self.Label6 = Label(top)
        self.Label6.place(relx=0.18, rely=0.01, height=18, width=127)
        self.Label6.configure(activebackground="#f9f9f9")
        self.Label6.configure(font=font9)
        self.Label6.configure(text='''Import Data''')
        
        #Button Close for exiting the application
        self.Button_Close = Button(top)
        self.Button_Close.place(relx=0.42, rely=0.94, height=26, width=127)
        self.Button_Close.configure(activebackground="#d9d9d9")
        self.Button_Close.configure(command=support.closeApllication)
        self.Button_Close.configure(text='''Close''')



if __name__ == '__main__':
    
    vp_start_gui()



