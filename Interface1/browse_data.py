#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 19 16:34:43 2018
This python file is used to upload CSV files into the MongoDB database.
@author: shabani
"""

import sys
import os
import time
import datetime
from datetime import datetime
from pymongo import MongoClient,ReadPreference
import pandas as pd
import json



try:
    from Tkinter import *
except ImportError:
    from tkinter import *

try:
    import ttk
    py3 = False
except ImportError:
    import tkinter.ttk as ttk
    py3 = True

from tkinter import filedialog, messagebox

#fonction to import a Csv file to the mongodb
def mongoimportCSV(csv_path, db_name, coll_name,separator):
    if(separator==""):
        separator=","
    client = MongoClient() #connecting with database
    db = client[db_name] #the database name
    coll = db[coll_name] #collection name
    data = pd.read_csv(csv_path,sep=separator) #read the csv file
    #print('data='+str(data))
    data.columns = [col.replace(".1", "_Copy") for col in data.columns] #split the "." for duplicate columns

    #dat = pd.DataFrame(data)
    #print(dat)
    #data = data.drop('VOL_GAZ_CHA_I_001.1', 1) 
    #data = data.drop('VOL_ECS_PUI_I_001.1', 1)
    #print(data.columns)
    #print('data='+str(data.to_json(orient='records')))
    payload = json.loads(data.to_json(orient='records')) #convert csv file to json
    coll.remove() #remove the collection if it exists
    coll.insert(payload,check_keys=False) #insert in the collection the json file


#function to import Csv files to the mongodb from a folder
def mongoimportFolder(file_path, db_name, coll_name,separator):
    if(separator==""):
        separator=","
    client = MongoClient() #connecting with database
    db = client[db_name] #the database name
    coll = db[coll_name] #collection name
    coll.remove() #remove collection
    count = 0 #variable to count number of Csv files
    res=0 #variable to count the current inserting number
    for dossier,sous_dossier,fichiers in os.walk(file_path): #for every csv file in folders
        for fichier in fichiers:
            count = count +1 #count number of Csv files
            print(count)
    for dossier,sous_dossier,fichiers in os.walk(file_path): #for every csv file in folders
        for fichier in fichiers:
            res= res+1 #count current number
            uploading="Loaded " +str(res)+"/"+str(count)
            print(uploading) #print the progress
            #messagebox.showinfo("Importating ", uploading)
            list=os.path.join(dossier,fichier) # CSV file path 
            data = pd.read_csv(list, sep=separator) #read the Csv from the path
            data.columns = [col.replace("[", "") for col in data.columns]
            data.columns = [col.replace("]", "") for col in data.columns]
            data.columns = [col.replace(" ","_") for col in data.columns]
            #print(data)
            payload = json.loads(data.to_json(orient='records')) #convert csv file to json
            coll.insert(payload,check_keys=False)  #insert in the collection the json file